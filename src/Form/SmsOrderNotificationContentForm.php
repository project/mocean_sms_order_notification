<?php

namespace Drupal\mocean_sms_order_notification\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\Core\Routing\RequestContext;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\mocean_sms_order_notification\Utility;

class SmsOrderNotificationContentForm extends ConfigFormBase {

  /**
   * The route builder.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routeBuilder;

  /**
   * The request context.
   *
   * @var \Drupal\Core\Routing\RequestContext
   */
  protected $requestContext;

  /**
   * Constructs a SmsOrderNotificationContentForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Routing\RouteBuilderInterface $route_builder
   *   The route builder.
   * @param \Drupal\Core\Routing\RequestContext $request_context
   *   The request context.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RouteBuilderInterface $route_builder, RequestContext $request_context) {
    parent::__construct($config_factory);
    $this->routeBuilder = $route_builder;
    $this->requestContext = $request_context;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('router.builder'),
      $container->get('router.request_context')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mocean_sms_notification_content_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
	$form = parent::buildForm($form, $form_state);
    $sms_order_notification_content = $this->config('mocean_sms_order_notification.content');

	$form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('Manage content of SMS to be sent on shipping events.'),
    ];

    $form['content'] = [
      '#type' => 'vertical_tabs',
      '#default_tab' => 'edit-publication',
    ];

    $form['ready'] = [
      '#type' => 'details',
      '#title' => $this->t('Ready for shipping'),
      '#group' => 'content',
	  '#tree' => TRUE,
    ];

    $form['ready']['ready_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
	  '#default_value' => $sms_order_notification_content->get('ready_enable'),
    ];

	$form['ready']['ready_content'] = [
      '#type' => 'textarea',
	  '#cols'  => 60,
      '#rows'  => 5,
      '#title' => $this->t('Content'),
	  '#default_value' => $sms_order_notification_content->get('ready_content'),
      '#description' => $this->t('Available placeholders: {store_name}, {store_email}, {order_number}, {order_track_code}'),
    ];

    $form['shipped'] = [
      '#type' => 'details',
      '#title' => $this->t('Shipped'),
      '#group' => 'content',
	  '#tree' => TRUE,
    ];

	$form['shipped']['shipped_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
	  '#default_value' => $sms_order_notification_content->get('shipped_enable'),
    ];

    $form['shipped']['shipped_content'] = [
      '#type' => 'textarea',
	  '#cols'  => 60,
      '#rows'  => 5,
      '#title' => $this->t('Content'),
	  '#default_value' => $sms_order_notification_content->get('shipped_content'),
      '#description' => $this->t('Available placeholders: {store_name}, {store_email}, {order_number}, {order_track_code}'),
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    $form['#attached']['library'][] = 'mocean_sms_order_notification/mocean_sms_order_notification';

	  return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
	$this->config('mocean_sms_order_notification.content')
      ->set('ready_content', $form_state->getValue(['ready','ready_content']))
      ->set('ready_enable', $form_state->getValue(['ready','ready_enable']))
      ->set('shipped_content', $form_state->getValue(['shipped','shipped_content']))
      ->set('shipped_enable', $form_state->getValue(['shipped','shipped_enable']))
      ->save();

    \Drupal::messenger()->addStatus($this->t('Content saved.'));
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['mocean_sms_order_notification.content'];
  }

}
