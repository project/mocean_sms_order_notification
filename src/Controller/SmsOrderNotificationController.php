<?php

namespace Drupal\mocean_sms_order_notification\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller for mocean_sms_broadcast pages.
 */
class SmsOrderNotificationController extends ControllerBase {
	
  public function smsOrderNotificationContent() {
    $build = [];
    $build['mocean_sms_notification_settings_form'] = $this->formBuilder()->getForm('Drupal\mocean_sms_order_notification\Form\SmsOrderNotificationContentForm');
    return $build;
  }
  
}
