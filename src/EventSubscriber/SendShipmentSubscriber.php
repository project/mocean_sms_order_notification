<?php

namespace Drupal\mocean_sms_order_notification\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Drupal\mocean_sms_order_notification\Utility;

class SendShipmentSubscriber implements EventSubscriberInterface {

  public static function getSubscribedEvents() {
    $events = ['commerce_shipment.ship.post_transition' => 'onSend'];
	return $events;
  }

  public function onSend(WorkflowTransitionEvent $event) {
	$sms_order_notification_content = \Drupal::config('mocean_sms_order_notification.content');
	$enable = $sms_order_notification_content->get('shipped_enable');
    $message =  $sms_order_notification_content->get('shipped_content');
	$order = $event->getEntity();
    
    $recipient = $order->getOrder()->getCustomer()->get((new Utility)->getFieldName())->value;
    $store_name = $order->getOrder()->getStore()->getName();
    $store_email = $order->getOrder()->getStore()->getEmail();
    $order_number = $order->getOrder()->getOrderNumber();
    $order_track = $order->getTrackingCode();

	if (!empty($recipient) && $enable) {
      
      $placeholder = ['{store_name}', '{store_email}', '{order_number}', '{order_track_code}'];
      $replace = [$store_name, $store_email, $order_number, $order_track];
      $message = str_replace($placeholder, $replace, $message);
      \Drupal::logger('mocean_sms_order_notification')->notice($message);
	
	  $jsonResponse = (new Utility)->smsOrderNotificationSendMessage($recipient, $message);
	  
	  if (count($jsonResponse) == count($jsonResponse, COUNT_RECURSIVE)) {
	    $status = $jsonResponse['status'];
	    $is_arr = FALSE;
	  }
	  else {
	    $status = $jsonResponse['messages'][0]['status'];
	    $is_arr = TRUE;
	  }
	
	  if ($status != 0)
	    if ($is_arr)
          \Drupal::logger('mocean_sms_order_notification')->error('Notification SMS could not be sent for order number '.$order_number.'. '.$jsonResponse['messages'][0]['err_msg']);
	    else
          \Drupal::logger('mocean_sms_order_notification')->error('Notification SMS could not be sent for order number '.$order_number.'. '.$jsonResponse['err_msg']);
    }
  }
}