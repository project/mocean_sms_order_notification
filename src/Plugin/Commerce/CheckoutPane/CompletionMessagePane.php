<?php

namespace Drupal\mocean_sms_order_notification\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\mocean_sms_order_notification\Utility;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a completion SMS pane.
 *
 * @CommerceCheckoutPane(
 *   id = "checkout_pane_complete_order",
 *   label = @Translation("Completion SMS"),
 *   default_step = "_disabled",
 * )
 */
class CompletionMessagePane extends CheckoutPaneBase {
	
  
  public function defaultConfiguration() {
    return [
      'content' => 'Your order with order number {order_number} and total charge of {total_price} has been placed at {store_name}.',
    ] + parent::defaultConfiguration();
  }

  public function buildConfigurationSummary() {
    return $this->t('Content: @content', ['@content' => $this->configuration['content']]);
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['content'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SMS Content'),
      '#default_value' => $this->configuration['content'],
      '#description' => $this->t('Available placeholders: {store_name}, {store_email}, {order_number}, {total_price}'),
    ];

    return $form;
  }

  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['content'] = $values['content'];
    }
  }

  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $message =  $this->configuration['content'];
    
	$recipient = $this->order->getCustomer()->get((new Utility)->getFieldName())->value;
    $store_name = $this->order->getStore()->getName();
    $store_email = $this->order->getStore()->getEmail();
    $order_number = $this->order->getOrderNumber();
    $total_price = $this->order->getTotalPrice();

	if ($recipient) {
      $placeholder = ['{store_name}', '{store_email}', '{order_number}', '{total_price}'];
      $replace = [$store_name, $store_email, $order_number, $total_price];
      $message = str_replace($placeholder, $replace, $message);
      
      /*
	  $message = str_replace('{store_name}', $this->order->getStore()->getName(), $this->configuration['content']);
	  $message = str_replace('{total_price}', $this->order->getTotalPrice(), $message);
      */
	  $jsonResponse = (new Utility)->smsOrderNotificationSendMessage($recipient, $message);
	  
	  if (count($jsonResponse) == count($jsonResponse, COUNT_RECURSIVE)) {
	    $status = $jsonResponse['status'];
	    $is_arr = FALSE;
	  }
	  else {
	    $status = $jsonResponse['messages'][0]['status'];
	    $is_arr = TRUE;
	  }
	
	  if ($status != 0) {
	    if ($is_arr)
          \Drupal::logger('mocean_sms_order_notification')->error('Notification SMS could not be sent for order number '.$order_number.'. '.$jsonResponse['messages'][0]['err_msg']);
	    else
          \Drupal::logger('mocean_sms_order_notification')->error('Notification SMS could not be sent for order number '.$order_number.'. '.$jsonResponse['err_msg']);
      }
	  
	  return $pane_form;
    }
  }
}